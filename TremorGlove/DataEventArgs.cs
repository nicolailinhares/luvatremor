﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TremorGlove
{
    public class DataEventArgs: EventArgs
    {
        public int[] Data {get; set;}
        public TremorGloveBoard Board { get; set; }

    }
}
