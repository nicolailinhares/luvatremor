﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TremorGlove
{
    class CBuffer<T>
    {
        private List<T> data;
        public CBuffer(){
            data = new List<T>();
        }
        public int Length { get {return data.Count; } }
        public void Write(T[] segment) {
            lock (data)
            {
                data.AddRange(segment);
            }
            
        }
        public T[] Read(int q) {
            lock (data)
            {   
                if (data.Count < q)
                    return new T[0];
                if (q == 0)
                {
                    int rest = data.Count % 24;
                    q = data.Count - rest;
                }
                    
                T[] dados = new T[q];
                dados = data.Take(q).ToArray();
                data.RemoveRange(0, q);        
                return dados;
            }
        }
    }
}
