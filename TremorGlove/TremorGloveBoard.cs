﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
namespace TremorGlove
{
    public delegate void DataAvailableEvent(object sender, EventArgs e);
    public class TremorGloveBoard
    {
        //fazer enum das boardcommands
        private static string[] strCommands = { "T", "S", "QQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQQ" };
        private SerialPort port;
        private static TremorGloveBoard instance;
        private Thread readingThread;
        private const int CHECKRESP = '2';
        public static List<BoardChannels> AvailableChannels = new List<BoardChannels>(new BoardChannels[] { BoardChannels.Analog1, BoardChannels.Analog2, BoardChannels.Analog3, BoardChannels.Analog4, BoardChannels.Analog5, BoardChannels.Analog6, BoardChannels.Accelerometer1, BoardChannels.Accelerometer2, BoardChannels.Magnetometer1, BoardChannels.Magnetometer2, BoardChannels.Gyroscope1, BoardChannels.Gyroscope2 });
        private CBuffer<int> Data;
        public bool Reading { get; private set; }
        public string PortName { get { return port.PortName; } }
        private TremorGloveBoard()
        {
            port = new SerialPort();
            port.BaudRate = 115200;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
            port.DataBits = 8;
            port.NewLine = "\r\n";
            port.ReadTimeout = 200;
            port.ReadBufferSize = 8192;
            Data = new CBuffer<int>();
            Reading = false;
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        { 
            int bytesr = port.BytesToRead;
            int rest = bytesr % 2;
            byte[] buff = new byte[bytesr - rest];
            int[] values = new int[buff.Length / 2];
            int cont = 0;
            port.Read(buff, 0, buff.Length);
            for (int i = 0; i < buff.Length; i += 2)
                values[cont++] = BitConverter.ToInt16(buff, i);//(buff[i+1] << 8) + buff[i];
            Data.Write(values);
                //Thread.Sleep(100);
        }

        public enum BoardCommands
	    {
	        Check = 0,
            Start = 1,
            Stop = 2     
	    }

        public enum MagnetOptions 
        {
            M1 = 1, M2, M3, M4, M5, M6, M7
        }

        public enum GyroOptions
        {
            G1 = 1, G2, G3
        }

        public enum AccelerOptions
        {
            A1 = 1, A2, A3
        }

        public enum BoardChannels
        {
            Analog1 = 0, Analog2, Analog3, Analog4, Analog5, Analog6,
            Accelerometer1, Accelerometer2 = 9, Magnetometer1 = 12, Magnetometer2 = 15, Gyroscope1 = 18, Gyroscope2 = 21 
        }

        public static int SeriesNumber(BoardChannels chn){
            if (chn < BoardChannels.Accelerometer1)
                return 1;
            else
                return 3;
        }

        private int SendCommand(BoardCommands com){
            string rline = "\0";
            string line = strCommands[(int)com];
            port.WriteLine(line);
            try
            {
                rline = port.ReadLine();
            }
            catch (TimeoutException) {
                rline = "\0";
            }
            if (rline == "")
                return 0;
            return rline.ToCharArray(0,1)[0];
        }

        public static TremorGloveBoard Detect(){
            if (instance == null)
            {
                instance = new TremorGloveBoard();
                string[] names = SerialPort.GetPortNames();
                int resp = 0;
                foreach(string name in names){
                    instance.port.PortName = name;
                    try
                    {
                        instance.port.Open();
                    }
                    catch (UnauthorizedAccessException)
                    {
                        continue;
                    }
                    resp = instance.SendCommand(BoardCommands.Check);
                    instance.port.Close();
                    if (resp == CHECKRESP)
                        break;
                }
                if (resp == 0)
                    instance = null;
                //instance.port.PortName = "COM8";
            }
            return instance;
        }

        public void Start()
        {
            port.ReceivedBytesThreshold = 240;
            port.DataReceived += port_DataReceived;
            port.Open();
            port.DiscardInBuffer();
            SendCommand(BoardCommands.Start);
            Reading = true;
            //readingThread = new Thread(new ThreadStart(DataProcessing));
            //readingThread.Start();
        }

        public void Configure(AccelerOptions acc, MagnetOptions magn, GyroOptions gyro)
        {
            port.Open();
            /*port.WriteLine(acc.ToString());
            port.WriteLine(magn.ToString());
            port.WriteLine(gyro.ToString());*/
            port.Close();
        }

        public int[] ReadData(int q)
        {
            return Data.Read(q);
        }

        public void Stop()
        {
            Reading = false;
            //readingThread.Join();
            SendCommand(BoardCommands.Stop);
            port.Close();
        }

    }
}
