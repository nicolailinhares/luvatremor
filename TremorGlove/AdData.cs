﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TremorGlove
{
    public delegate void ScannedEventHandler(object sender, DataEventArgs e);
    public class AdData
    {
        public event ScannedEventHandler Scanned;
        private TremorGloveBoard board;
        private object ThisLock;
        public bool Reading { get; private set; }
        private Thread readingThread;
        public AdData(TremorGloveBoard b)
        {
            board = b;
            ThisLock = new object();
            Reading = false;
        }
        public void ReadBuffer()
        {
            while (Reading)
            {
                int[] data = board.ReadData(0);
                if (data.Length > 0)
                {
                    DataEventArgs de = new DataEventArgs();
                    de.Data = data;
                    de.Board = board;
                    Scanned(this, de);
                }
                Thread.Sleep(100);
            }
        }

        /*public void SplitData(object sender, DoWorkEventArgs e)
        {
            
        }*/

        public void Start()
        {
            board.Start();
            readingThread = new Thread(new ThreadStart(ReadBuffer));
            Reading = true;
            readingThread.Start();
        }

        public void ConfigureBoard(TremorGloveBoard.AccelerOptions acc, TremorGloveBoard.MagnetOptions magn, TremorGloveBoard.GyroOptions gyro)
        {
            board.Configure(acc, magn, gyro);
        }

        public void Stop()
        {
            if (board.Reading)
            {
                board.Stop();
            }
            Reading = false;
            readingThread.Join(20);
        }
    }
}
