﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MultiData_Acq.Util
{
    class BoardErrorException : Exception
    {
        public object Sender { get; set; }
        public string ErrMessage { get; set; }
        public BoardErrorException(object sender, string msg)
        {
            Sender = sender;
            ErrMessage = msg;
        }

        public static void TestException(object sender, string msg)
        {
            throw new BoardErrorException(sender, msg);
        }
    }
}
