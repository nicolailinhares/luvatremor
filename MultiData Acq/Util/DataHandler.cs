﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace MultiData_Acq.Util
{
    public delegate void FinishedEventHandler(object sender, EventArgs e);
    public delegate void DataProcessingHandler(TremorGlove.DataEventArgs e);
    class DataHandler
    {
        private int maxPontos;
        private int pontos;
        private TremorGlove.AdData dataCont;
        private BoardConfiguration boardConfig;
        private FileHandler fileHand;
        private PlotHandler plotHand;
        private List<ChannelControl> models;
        private Dispatcher uiDispatcher;
        public bool IsRunning { get; private set; }
        public DataHandler(TremorGlove.TremorGloveBoard board, BoardConfiguration bc, Dispatcher ui)
        {
            dataCont = new TremorGlove.AdData(board);
            models = new List<ChannelControl>();
            uiDispatcher = ui;
            boardConfig = bc;
        }

        public void Start(ColetaInfo ci)
        {
            List<string> names = SetChannelNames();            
            maxPontos = ci.Duration * 100 * 24;
            fileHand = new FileHandler(24, "TremorGlove", 100, ci.PatientName, names, string.Format("{0}, {1}, {2}", boardConfig.AccSense, boardConfig.MagnSense, boardConfig.GyroSense));
            plotHand = new PlotHandler(models, uiDispatcher);
            Processing += fileHand.CreateBackground;
            Processing += plotHand.CreateBackground;
            dataCont.Scanned += DispatchData;
            dataCont.ConfigureBoard(boardConfig.AccSense, boardConfig.MagnSense, boardConfig.GyroSense);
            dataCont.Start();
            IsRunning = true;
        }

        public void Stop()
        {
            if (dataCont.Reading)
            {
                dataCont.Stop();
            }
            Processing -= fileHand.CreateBackground;
            Processing -= plotHand.CreateBackground;
            dataCont.Scanned -= DispatchData;
            IsRunning = false;
        }

        public event FinishedEventHandler Finished;

        protected virtual void OnFinished(EventArgs e)
        {
            if (Finished != null)
                Finished(this, e);
        }

        public event DataProcessingHandler Processing;

        private void DispatchData(object sender, TremorGlove.DataEventArgs e)
        {
            if (maxPontos > 0 && pontos >= maxPontos)
            {
                Stop();
                uiDispatcher.Invoke(() =>
                {
                    OnFinished(EventArgs.Empty);
                });
            }
            else
            {
                Processing.Invoke(e);
                pontos += e.Data.Length;
            }         
        }
        public void AddPlotModel(ChannelControl pm)
        {
            models.Add(pm);
        }

        private List<string> SetChannelNames()
        {
            List<string> names = new List<string>();
            string[] axes = { "X", "Y", "Z" };
            TremorGlove.TremorGloveBoard.AvailableChannels.ForEach(delegate(TremorGlove.TremorGloveBoard.BoardChannels ch)
            {
                int qdata = TremorGlove.TremorGloveBoard.SeriesNumber(ch);
                if (qdata > 1)
                {
                    for (int i = 0; i < qdata; i++)
                    {
                        names.Add(string.Format("{0}.{1}", ch.ToString(), axes[i]));
                    }
                }
                else
                {
                    names.Add(ch.ToString());
                }
            });
            models.ForEach(delegate(ChannelControl cc)
            {
                int qdata = TremorGlove.TremorGloveBoard.SeriesNumber(cc.ChannelType);
                if (qdata > 1)
                {
                    for (int i = 0; i < qdata; i++)
                    {
                        names[((int)cc.ChannelType) + i] = string.Format("{0}.{1}", cc.ChnName, axes[i]);
                    }
                }
                else
                {
                    names[(int)cc.ChannelType] = cc.ChnName;
                }

                cc.DisableClick();
            });
            return names;
        }
    }
}
