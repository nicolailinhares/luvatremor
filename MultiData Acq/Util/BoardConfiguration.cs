﻿using System;
using System.Collections.Generic;
using TremorGlove;
namespace MultiData_Acq.Util
{
    public class BoardConfiguration
    {
        public int Num { get; set; }
        public int Rate {get; set;}
        public int MaxChannels { get; set; }
        public TremorGloveBoard.AccelerOptions AccSense{get; set;}
        public TremorGloveBoard.MagnetOptions MagnSense { get; set; }
        public TremorGloveBoard.GyroOptions GyroSense { get; set; }
        public List<TremorGloveBoard.BoardChannels> SelectedChannels { get; private set; }
        public BoardConfiguration(int lC, int qC, int r)
        {
            SelectedChannels = new List<TremorGloveBoard.BoardChannels>();//TremorGloveBoard.AvailableChannels;
            MaxChannels = 12;
            Rate = 100;
            AccSense = TremorGloveBoard.AccelerOptions.A1;
            MagnSense = TremorGloveBoard.MagnetOptions.M1;
            GyroSense = TremorGloveBoard.GyroOptions.G1;
        }

        public string BoardName
        {
            get{return String.Format("Board {0}", Num);}
        }
    }
}
