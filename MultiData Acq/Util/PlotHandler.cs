﻿using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace MultiData_Acq.Util
{
    class PlotHandler : AbstractHandler
    {
        private List<ChannelControl> channels;
        private Dispatcher uiDispatcher;
        private int count;
        private int qChans;
        private const int MAX = 500;
        public PlotHandler(List<ChannelControl> ms, Dispatcher ui){
            channels = ms;
            qChans = channels.Count;
            uiDispatcher = ui;
        }
        public override void Handling(int[] data, TremorGlove.TremorGloveBoard Board)
        {
            uiDispatcher.Invoke(() =>
			{
                reading = true;
                int q = data.Length / 24;
                foreach(ChannelControl chn in channels){
                    for (int i = 0; i < TremorGlove.TremorGloveBoard.SeriesNumber(chn.ChannelType); i++)
                    {
                        var lineSeries = chn.PlotModel.Series[i] as LineSeries;
                        if (count + q >= MAX)
                        {
                            lineSeries.Points.Clear();
                        }
                        else
                        {
                            for (int j = 0; j < q; j+=4)
                            {
                                lineSeries.Points.Add(new DataPoint(count + j, data[(int)chn.ChannelType + i + j * 24]));
                            }
                        }
                    }
                    chn.PlotModel.InvalidatePlot(true); 
                }
                count += q;
                if (count >= MAX)
                    count = 0;
                /*if (count >= MAX)
                {
                    foreach (ChannelControl chn in channels)
                    {
                        chn.PlotModel.
                    }
                    count = 0;
                }*/
                    

			});
                
        }

    }
}
