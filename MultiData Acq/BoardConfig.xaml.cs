﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MultiData_Acq.Util;
namespace MultiData_Acq
{
    /// <summary>
    /// Interaction logic for BoardConfig.xaml
    /// </summary>
    public partial class BoardConfig : UserControl
    {
        public BoardConfiguration BoardProperties{
            get{
                boardProperties.AccSense = (TremorGlove.TremorGloveBoard.AccelerOptions)(3 - AccSlider.Value + 1);
                boardProperties.MagnSense = (TremorGlove.TremorGloveBoard.MagnetOptions)(7 - MagnSlider.Value + 1);
                boardProperties.GyroSense = (TremorGlove.TremorGloveBoard.GyroOptions)(3 - GyroSlider.Value + 1);
                return boardProperties;
            }
            set { boardProperties = value; }
        }
        private BoardConfiguration boardProperties;
        public BoardConfig(BoardConfiguration bc)
        {
            InitializeComponent();
            num.Text = bc.BoardName;
            BoardProperties = bc;
        }

        private void Chn_Click(object sender, RoutedEventArgs e)
        {
            CheckBox cb = (CheckBox)sender;
            int chnpos = (int) cb.GetValue(Grid.RowProperty);
            TremorGlove.TremorGloveBoard.BoardChannels brdchn = (TremorGlove.TremorGloveBoard.BoardChannels) chnpos;
            if (cb.IsChecked == true)
            {
                BoardProperties.SelectedChannels.Add(brdchn);
                BoardProperties.SelectedChannels.Sort();
            }
            else
                BoardProperties.SelectedChannels.Remove(brdchn);
        }
        
    }
}
